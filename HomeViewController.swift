//
//  HomeViewController.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/4/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

/**
 City struct for cities whose weather data is being searched
 
 - parameter name: Name of the city
 - parameter id: City ID
 */
private struct City {
    var name: String
    var id: String
}

private let reuseIdentifier = "HomeCellIdentifier"
private let segueIdentifier = "ShowDetail"

class HomeViewController: UICollectionViewController {
    
    var activityIndicator: CBSWActivityIndicator?
    var cityWeatherResults = [CityWeather]()
    private let cityItems: [City] = [
        City(name: "San Francisco", id: Constants.CityIdentifiers.SAN_FRANCISCO),
        City(name: "New York",      id: Constants.CityIdentifiers.NEW_YORK),
        City(name: "London",        id: Constants.CityIdentifiers.LONDON),
        City(name: "Cairo",         id: Constants.CityIdentifiers.CAIRO),
        City(name: "Tokyo",         id: Constants.CityIdentifiers.TOKYO)
    ]
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearsSelectionOnViewWillAppear = false
        collectionView?.collectionViewLayout = flowLayout()
        activityIndicator = CBSWActivityIndicator()
        activityIndicator!.startAnimating()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var cities = [String]()
        for item in cityItems {
            cities.append(item.id)
        }
        
        /* There are a billion ideas I had for this... Pull-to-refresh whenever user wants to update the weather, a button with that same implementation at the bottom, etc.
        *   Decided to update the weather everytime the user returns to the home screen. Probably not that ideal if the operation were a bit heavier, but for this it works well
        */
        fetchCurrentWeatherData(cities)
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityWeatherResults.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! HomeViewControllerCollectionViewCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let detailViewController = instantiateViewController(DetailViewController)
        detailViewController!.cityWeatherItem = cityWeatherResults[indexPath.item]
        showViewController(detailViewController!, sender: self)
    }
    
    // MARK: Collection View Helper methods
    
    private func configureCell(cell: HomeViewControllerCollectionViewCell, atIndexPath indexPath: NSIndexPath) {
        let cityWeatherResult = cityWeatherResults[indexPath.item]
        cell.cityLabel.text = cityWeatherResult.cityName!
        cell.temperatureLabel.text = "\(cityWeatherResult.currentTemperature!)"
        cell.contentContainerView.backgroundColor = UIColor.temperatureColor(cityWeatherResult.currentTemperature!)
        cell.weatherIconView.weatherIcon(code: cityWeatherResult.weatherIcon!) // Peek in the extensions file to see what's going on here
    }
    
    private func flowLayout() -> UICollectionViewFlowLayout {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        flowLayout.itemSize = Constants.Device.isIphone ? CGSizeMake(view.frame.size.width, 120) : CGSizeMake(splitViewController!.viewControllers.first!.view.frame.width / 3.2, 100)
        return flowLayout
    }
    
    // MARK: Networking
    
    private func fetchCurrentWeatherData(cities: [String]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            CBSWeatherAPI.fetchCurrentWeatherData(cities: cities) { (result, error) -> () in
                if error == nil {
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        self.cityWeatherResults = CBSParser.convertResultsToWeatherObjects(result!)!
                        self.collectionView!.reloadSections(NSIndexSet(index: 0))
                        self.activityIndicator?.stopAnimating()
                    })
                }
                else {
                    print("Error: \(error?.localizedDescription)")
                    self.showAlert("Network Error", "There was an error processing your request")
                    self.activityIndicator?.stopAnimating()
                }
            }
        }
    }

}

//MARK: HomeViewControllerCollectionViewCell

class HomeViewControllerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentContainerView: UIView!
    @IBOutlet weak var weatherIconView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupAppearance()
    }
    
    private func setupAppearance() {
        contentContainerView.layer.shadowRadius = 2.0
        contentContainerView.layer.shadowOpacity = 0.4
        contentContainerView.layer.shadowColor = UIColor.blackColor().CGColor
        contentContainerView.layer.shadowOffset = CGSizeMake(0, 3)
    }
}

