//
//  DetailViewController.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/4/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

private let reuseIdentifier = "DayCellIdentifier"
private let headerIdentifier = "HeaderIdentifier"

class DetailViewController: UITableViewController {
    
    //// Constants
    let headerRowHeight:     CGFloat = 100
    let tableViewCellHeight: CGFloat = 80
    
    //// Properties
    var cityWeatherItem: CityWeather?
    var forecastItems = [ForecastItem]()
    
    //// Outlets
    @IBOutlet weak var navigationBarTitleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchWeatherData()
    }
    
    // MARK: Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarTitleLabel.text = cityWeatherItem?.cityName
        view.backgroundColor = UIColor.temperatureColor(cityWeatherItem!.currentTemperature!)
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastItems.count + 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
                let headerView = tableView.dequeueReusableCellWithIdentifier(headerIdentifier) as! HeaderView
                configureHeaderView(headerView)
                return headerView
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! DetailTableViewCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return headerRowHeight
        }
        
        return tableViewCellHeight
    }
    
    // MARK: Configure cell helper methods
    
    private func configureHeaderView(headerView: HeaderView) {
        headerView.weatherIconView.weatherIcon(code: cityWeatherItem!.weatherIcon!)
        headerView.temperatureLabel.text = "\(cityWeatherItem!.currentTemperature!)"
    }
    
    private func configureCell(cell: DetailTableViewCell, indexPath: NSIndexPath) {
        let forecastItem = forecastItems[indexPath.item - 1]
        cell.dayNameLabel.text = indexPath.item == 1 ? "Tomorrow" : forecastItem.date!.weekday()
        cell.temperatureLabel.text = "\(forecastItem.temperature!)"
    }
    
    // MARK: Helpers
    
    private func fetchWeatherData() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            CBSWeatherAPI.fetchForecast(city: self.cityWeatherItem!.cityName!, numberOfDays: 8) { (result, error) -> Void in
                if error == nil {
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        self.forecastItems = CBSParser.convertResultsToForecastItems(result!)!
                        self.forecastItems.removeFirst() // the data in the first index is the weather data for today.. we don't need that since we're passing that info in already
                        self.tableView.reloadData()
                    })
                } else {
                    self.showAlert("Error", "There was an error processing your request")
                    print("Error \(error?.localizedDescription)")
                }
            }
        }
    }

}

/**
Custom Table View cell that displays weather data
*/
class DetailTableViewCell: UITableViewCell {
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
}

/**
The section header view
*/
class HeaderView: UITableViewCell {
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherIconView: UIImageView!
}
