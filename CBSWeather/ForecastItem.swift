//
//  ForecastItem.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/6/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

class ForecastItem: NSObject {
    var date: NSTimeInterval?
    var temperature: Int?
    
    convenience init(date: NSTimeInterval, temperature: Int) {
        self.init()
        self.date = date
        self.temperature = temperature
    }
}
