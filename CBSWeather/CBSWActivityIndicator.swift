//
//  CBSWActivityIndicator.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/7/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

class CBSWActivityIndicator: UIActivityIndicatorView {

    init() {
        super.init(activityIndicatorStyle: .WhiteLarge)
        backgroundColor = UIColor.blackColor()
        layer.cornerRadius = 10.0
        frame = CGRectMake(0, 0, 80, 80)
        let applicationWindow = UIApplication.sharedApplication().windows.last!
        center = applicationWindow.center
        applicationWindow.addSubview(self)
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
