//
//  Constants.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/5/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

/**
Constants that will be used throughout the implementation of the app
*/
public class Constants {
    
    /**
     Constants related to device
     */
    public class Device {
        private static let deviceName = UIDevice.currentDevice().name
        private static let iPad = "iPad"
        private static let iPad_Simulator = "iPad Simulator"
        private static let iPhone = "iPhone"
        private static let iPhone_Simulator = "iPhone Simulator"
        public static let isIpad = deviceName == iPad || deviceName == iPad_Simulator
        public static let isIphone = deviceName == iPhone || deviceName == iPhone_Simulator
    }
    
    /**
    The constants that are specific to making network requests
    */
    public class Networking {
        
        // MARK: URI params
        public static let WEATHER_PARAM =   "weather?"
        public static let FORECAST_PARAM =  "forecast?"
        public static let CITY_PARAM =      "city?"
        public static let GROUP_PARAM =     "group?"
        public static let DAILY_PARAM =     "daily?"
        
        // MARK: baseURL
        public static let BASE_URL =        "http://api.openweathermap.org/data/2.5/"
    }
    
    /**
    The city identifiers associated with each city that will be inspected
    */
    public class CityIdentifiers {
        public static let TOKYO =           "1850147"
        public static let SAN_FRANCISCO =   "5391959"
        public static let NEW_YORK =        "5128638"
        public static let CAIRO =           "360630"
        public static let LONDON =          "2643743"
    }
}