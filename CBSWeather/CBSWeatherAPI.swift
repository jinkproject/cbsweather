//
//  CBSWeatherAPI.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/4/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit
import Alamofire

private let apiKey = "304470e367337ce35c323d523c19c2a6"

class CBSWeatherAPI: NSObject {
    
    /**
    Fetches the forecast for a city

    - parameter city: Name of the city being searched
    - parameter days: Number of days in the weather forecast data
    - parameter completion: Completion handler that either passes in the result dict or a request error
    */
    
    class func fetchForecast(city city: String, numberOfDays dayCount: Int = 1, completionHandler completion: ([String : AnyObject]?, NSError?) -> Void) {
        let urlString = Constants.Networking.BASE_URL + "forecast/daily?q=\(city.removeWhitespace()),US&cnt=\(dayCount)" + "&units=imperial&APPID=\(apiKey)"
        let url = NSURL(string: urlString)!
        Alamofire.request(.GET, url).responseJSON(options: .AllowFragments) { (response: Response<AnyObject, NSError>) -> Void in
            switch response.result {
            case .Success(let results):
                if let json = results as? [String: AnyObject] {
                    completion(json, nil)
                }
            case .Failure(let error):
                completion(nil, error)
            }
        }
    }
    
    class func fetchCurrentWeatherData(cities cities: [String], completionHandler completion: (result: [String: AnyObject]?, error: NSError?) -> ()) {
        let joinedCities: String = cities.joinWithSeparator(",")
        let urlString = Constants.Networking.BASE_URL + Constants.Networking.GROUP_PARAM + "id=\(joinedCities)&units=imperial&APPID=\(apiKey)"
        let url = NSURL(string: urlString)!
        
        Alamofire.request(.GET, url).responseJSON(options: .AllowFragments) { (response: Response<AnyObject, NSError>) -> Void in
            switch response.result {
            case .Success(let results):
                if let json = results as? [String: AnyObject] {
                    completion(result: json, error: nil)
                }
            case .Failure(let error):
                completion(result: nil, error: error)
            }
        }
    }
    
}

class CBSParser {
    
    /**
    Converts json into CityWeather models

    - parameter jsonDict: JSON to be converted
    */
    class func convertResultsToWeatherObjects(jsonDict: [String : AnyObject]) -> [CityWeather]? {
        var cityWeatherObjects = [CityWeather]()
        if let jsonArray = jsonDict["list"] as? [Dictionary<String, AnyObject>] { // I'm not breaking consistency here. Just want it to be noticeable that it's an array of dictionaries
            for element in jsonArray {
                guard let main = element["main"] as? [String : AnyObject] else {
                    return nil
                }
                
                let city: CityWeather = CityWeather(weatherDescription: nil, cityName: element["name"] as? String, highTemperature: main["temp_max"] as? Int, lowTemperature: main["temp_min"] as? Int, currentTemperature: main["temp"] as? Int, weatherIcon: nil)
                
                if let weatherParam = element["weather"] as? [Dictionary<String, AnyObject>] {
                    city.weatherIcon = weatherParam.last?["icon"] as? String
                }
                
                cityWeatherObjects.append(city)
            }
        }
        return cityWeatherObjects
    }

    /**
     Converts json into ForecastItem models
     
     - parameter jsonDict: JSON to be converted
     */
    class func convertResultsToForecastItems(jsonDict: [String : AnyObject]) -> [ForecastItem]? {
        var forecastItems = [ForecastItem]()
        if let resultsArray = jsonDict["list"] as? [Dictionary<String, AnyObject>] {
            for forecast in resultsArray {
                let date = forecast["dt"] as? NSTimeInterval
                if let dayTemp = forecast["temp"] as? Dictionary<String, AnyObject> {
                    let temperature = dayTemp["day"] as? Int
                    let forecastItem = ForecastItem(date: date!, temperature: temperature!)
                    forecastItems.append(forecastItem)
                }
                
            }
        }
        return forecastItems
    }
}
