//
//  CBSWNavigationController.swift
//  CBSWeather
//
//  Created by Steve Whitfield on 1/8/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

class CBSWNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return Constants.Device.isIpad ? .LandscapeLeft : .Portrait
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

}
