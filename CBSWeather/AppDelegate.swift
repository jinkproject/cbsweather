//
//  AppDelegate.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/4/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        // Using separate storyboards since these are two completely different UI setups
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        if Constants.Device.isIphone {
            let storyboard = UIStoryboard(name: "CBSWeather_iPhone", bundle: nil)
            let rootViewController = storyboard.instantiateInitialViewController() as! CBSWNavigationController
            rootViewController.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
            rootViewController.navigationBar.shadowImage = UIImage()
            rootViewController.navigationBar.translucent = true
            rootViewController.view.backgroundColor = UIColor.clearColor()
            rootViewController.navigationBar.barTintColor = UIColor.whiteColor()
            rootViewController.navigationBar.tintColor = UIColor.whiteColor()
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-300, -9999), forBarMetrics: .Default)
            window?.rootViewController = rootViewController
        } else {
            let storyboard = UIStoryboard(name: "CBSWeather_iPad", bundle: nil)
            let splitViewController = storyboard.instantiateInitialViewController() as! UISplitViewController
            window?.rootViewController = splitViewController
        }

        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

