//
//  Extensions.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/6/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    
    /**
     // Temp above 75F = red (RGB: 218,150,148)
     // Temp below 65F = blue (RGB: 198,217,240)
     // Temp between 74F-66F(inclusive) = peach (RGB: 252,213,181)
     */
    
    class func cbsWeatherBlue() -> UIColor {
        return UIColor(r: 198, g: 217, b: 240, a: 1)
    }
    
    class func cbsWeatherPeach() -> UIColor {
        return UIColor(r: 252, g: 213, b: 181, a: 1)
    }
    
    class func cbsWeatherRed() -> UIColor {
        return UIColor(r: 218, g: 150, b: 148, a: 1)
    }
    
    class func cbsWeatherBeige() -> UIColor {
        return UIColor(r: 238, g: 235, b: 225, a: 1)
    }
    
    class func temperatureColor(temperature: Int) -> UIColor {
        if temperature > 75 {
            return cbsWeatherRed()
        } else if temperature > 65 && temperature <= 75 {
            return cbsWeatherPeach()
        }
        
        return cbsWeatherBlue()
    }
}

extension String {
    func removeWhitespace() -> String {
        return stringByReplacingOccurrencesOfString(" ", withString: "")
    }
}

extension NSTimeInterval {
    /**
    Converts weekday date in unicode format to a 3-char string
    */
    func weekday() -> String {
        let formatter = NSDateFormatter()
        let dateFromString = NSDate(timeIntervalSince1970: self)
        formatter.dateStyle = .FullStyle
        formatter.dateFormat = "E"
        return formatter.stringFromDate(dateFromString)
    }
}

extension UIViewController {
    
    /**
     Displays alert with specified title & message
     
     - parameter title:   the title
     - parameter message: the message
     */
    func showAlert(title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,
            handler: { (_) -> Void in
                alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /**
    Instantiates view controller from its storyboard

    - parameter vc: View controller that will be created
    */
    func instantiateViewController<T: UIViewController>(vc: T.Type) -> T? {
        let viewController = NSStringFromClass(vc.self).componentsSeparatedByString(".").last
        return storyboard?.instantiateViewControllerWithIdentifier(viewController!) as? T
    }
    
}

private extension UIImage {
    convenience init?(string: String) {
        var imageData: NSData? = nil
        if let imageURL = NSURL(string: string) {
            imageData = NSData(contentsOfURL: imageURL)!
        }
        self.init(data: imageData!)!
    }
    
    class func imageData(URLString: String) -> NSData {
        var imageData: NSData? = nil
        if let imageURL = NSURL(string: URLString) {
            imageData = NSData(contentsOfURL: imageURL)!
        }
        
        return imageData!
    }
}


extension UIImageView {
    /**
     Searches cache for weather image key. If found, returns that image. If not, fetches image
     
     - parameter code: The weather icon code returned from a weather data request
     */
    func weatherIcon(code code: String) {
        let cache = CBSWCache.sharedCache
        if let cachedObject = cache.objectForKey(code) {
            self.image = UIImage(data: cachedObject as! NSData, scale: UIScreen.mainScreen().scale)
        } else {
            let imageURLString = "http://openweathermap.org/img/w/\(code).png"
            let imageData = UIImage.imageData(imageURLString)
            self.image = UIImage(data: imageData)
            cache.setObject(imageData, forKey: code)
        }
    }
}