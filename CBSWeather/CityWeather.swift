//
//  CityWeather.swift
//  CBSWeather
//
//  Created by Stephen Whitfield on 1/4/16.
//  Copyright © 2016 Stephen Whitfield. All rights reserved.
//

import UIKit

class CityWeather: NSObject {
    var weatherDescription: String?
    var cityName: String?
    var highTemperature: Int?
    var lowTemperature: Int?
    var currentTemperature: Int?
    var weatherIcon: String?
    
    /**
    Used to quickly and conveniently instantiate CityWeather model
    */
    convenience init(weatherDescription: String?, cityName: String?, highTemperature: Int?, lowTemperature: Int?, currentTemperature: Int?, weatherIcon: String?) {
        self.init()
        self.weatherDescription = weatherDescription
        self.cityName = cityName
        self.highTemperature = highTemperature
        self.lowTemperature = lowTemperature
        self.currentTemperature = currentTemperature
        self.weatherIcon = weatherIcon
    }
}
